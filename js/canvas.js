let inv = 300;
let size = 15;
let x = [];
let y = [];

let dot = [];
let xp = [];
let yp = [];

let c = document.getElementById("myCanvas");
let ctx = c.getContext("2d");
let ch = document.getElementById("checkDots");
let rd = document.getElementById("rast");
let dots = document.getElementById("dots").getElementsByTagName("div");

let razX = [];
let razY = [];


function fcanvas(){
    var scale = 1;
    var originx = 0;
    var originy = 0;

    ctx.clearRect(0, 0, c.width, c.height); // Limpa o canvas antes de desenhar
    ctx.save();                             // Salva as transformações
    ctx.scale(1, 1);          // Aplica a escala

    ctx.beginPath();
    ctx.strokeStyle = '#000000';
    ctx.lineWidth = 3;
    ctx.font='20px Arial';
    ctx.beginPath();
    for (let i = 0; i < dots.length; i++){
        xp[i] = document.getElementById("xp" + i).value;
        yp[i] = document.getElementById("yp" + i).value;
        if (xp[i] > 300){   xp[i] = 300;    }
        if (yp[i] > 300){   yp[i] = 300;    }
        ctx.lineTo(xp[i], inv-yp[i]);
    }
    ctx.lineTo(xp[0], inv-yp[0]);
    ctx.stroke();
    ctx.restore();                          // Reseta as transformações
}

function addPoint(xp, yp) {
    x.push(xp);
    y.push(yp);
}

/*
function zoom(dv) {
    zoomXY += dv;
    document.getElementById("myCanvas").style.transform = "scale(" + zoomXY + ")";
}

function escala(dv) {
    escalaXY += dv;
    fcanvas();
}
*/

function dot_place() {
    for (let i = 0; i < dots.length; i++){
        xp[i] = document.getElementById("xp" + i).value;
        yp[i] = document.getElementById("yp" + i).value;
        if (xp[i] > 300){   xp[i] = 300;    }
        if (yp[i] > 300){   yp[i] = 300;    }
        dot[i] = document.getElementById("dot" + i);
        if(xp[i] !== "" && yp[i] !== ""){
            dot[i].style.display = "block";
            dot[i].style.bottom = yp[i]-8 + "px";
            dot[i].style.left = xp[i]-4 + "px";
            dot[i].innerHTML = "<p>P"+ (i+1) +": ( " + xp[i] + ", " + yp[i] + " )</p>";
            addPoint(xp[i], yp[i]);
            fcanvas();
        }
    }
    clearRaz();
    for(let i = (dots.length-1); i > 0; i--){
        razMidPointer(xp[i-1],yp[i-1],xp[i],yp[i]);
    }
    if (dots.length > 2)
        razMidPointer(xp[0],yp[0], xp[dots.length-1],yp[dots.length-1]);
}

function limpar() {
    clearRaz();
    for(let i = 0; i < document.getElementById("table").getElementsByTagName("tr").length-1; i++){
        xp[i] = document.getElementById("xp" + i);
        yp[i] = document.getElementById("yp" + i);
        xp[i].value = "";
        yp[i].value = "";
        fcanvas();
    }
    for (let i = 0; i < dots.length; i++){
        dot[i].style.display = "none";
    }
}

function addPlace(ini = 1) {
    let pl = document.getElementById("table");
    let body = pl.getElementsByTagName("tbody");
    let size = pl.getElementsByTagName("tr").length - ini;
    body[0].innerText = "";
    document.getElementById("dots").innerText = "";
    for (let i = 0; i < size + 1; i++){
        let row = body[0].insertRow();
        row.id = "tr" + i;
        let cell_1 = row.insertCell(0);
        let cell_2 = row.insertCell(1);
        let cell_3 = row.insertCell(2);
        let cell_4 = row.insertCell(3);
        cell_1.innerHTML = "Ponto " + (i+1);
        cell_2.innerHTML = "<input id='xp" + i + "' type='number' min='0' max='300'>";
        cell_3.innerHTML = "<input id='yp" + i + "' type='number' min='0' max='300'>";
        cell_4.innerHTML = "<input type='button' value='remover' onclick='removePlace(" + i + ")'>";
        $("#dots").append("<div id='dot" + i + "' class='dot'></div>");
    }
    controlDots();
    limpar();
}

function removePlace(id) {
    let tr = document.getElementById("tr" + id);
    tr.parentNode.removeChild(tr);
    addPlace(2);
    fcanvas();
}

function hideDots() {
    for (let i = 0; i < dots.length; i++){
        dots[i].style.opacity = "0";
    }
}

function showDots() {
    for (let i = 0; i < dots.length; i++){
        dots[i].style.opacity = "1";
    }
}

function controlDots(){
    if (ch.checked){
        showDots();
    }else{
        hideDots();
    }
}

function showRast(){
    if (rd.checked){
        document.getElementsByClassName("tbl")[0].style.display = "inline-block";
    }else{
        document.getElementsByClassName("tbl")[0].style.display = "none";
    }
}

function razMidPointer(x0, y0, x1, y1) {
    let xi = parseInt(x0);
    let yi = parseInt(y0);
    let xf = parseInt(x1);
    let yf = parseInt(y1);

    xi *= (size/3)/100;
    xf *= (size/3)/100;
    yi *= (size/3)/100;
    yf *= (size/3)/100;

    xi = Math.round(xi);
    xf = Math.round(xf);
    yi = Math.round(yi);
    yf = Math.round(yf);
    console.log("x0: " + xi + "  y0: " + yi + "  x1: " + xf + "  y1: " + yf + "  size: " + size);

    let dx = xf - xi;
    let dy = yf - yi;
    let inclinacao = 0;

    if (dx < 0) {
        razMidPointer(x1,y1,x0,y0);
        return;
    }

    if (dy < 0)
        inclinacao = -1;
    else
        inclinacao = 1;

    let incE, incNE, d, cont = 0;

    let px = xi;
    let py = yi;

    razX = [];
    razY = [];

    paintPoint(xi,yi);

    if (dx >= inclinacao*dy){
        if (dy < 0){
            d = 2*dy + dx;
            while (px < xf){
                px++;
                if (d < 0){
                    d += 2*(dy+dx);
                    py--;
                } else {
                    d += 2*dy;
                }
                paintPoint(px ,py);
                razX[cont] = px;
                razY[cont] = py;
                cont++;
            }
        } else {
            d = 2*dy-dx;
            while (px < xf){
                px++;
                if (d < 0){
                    d += 2*dy;
                } else {
                    d += 2*(dy-dx);
                    py++;
                }
                paintPoint(px,py);
                razX[cont] = px;
                razY[cont] = py;
                cont++;
            }
        }
    } else {
        if (dy < 0){
            d = dy + 2*dx;
            while (py > yf){
                py--;
                if (d < 0){
                    d += 2*dx;
                } else {
                    d += 2*(dy+dx);
                    px++;
                }
                paintPoint(px,py);
                razX[cont] = px;
                razY[cont] = py;
                cont++;
            }
        } else {
            d = dy - 2*dx;
            while (py < yf){
                py++;
                if (d < 0){
                    d += 2*(dy-dx);
                    px++;
                } else {
                    d += -2*dx;
                }
                paintPoint(px,py);
                razX[cont] = px;
                razY[cont] = py;
                cont++;
            }
        }
        //paintPoint(xf,yf);
    }

    console.log("X:  " + razX.toString());
    console.log("Y:  " + razY.toString());
    console.log("X[]:  " + razY.length);
    console.log("Y[]:  " + razY.length);
}

function clearRaz() {
    for (let i = 0; i < size; i++){
        for (let j = 0; j < size; j++){
            document.getElementById("x"+j+"y"+i).style.background = "transparent";
        }
    }
}

function paintPoint(xt, yt){
    let xpp = Math.round(xt);
    let ypp = Math.round(yt);
    //console.log("X: " + xpp + "  Y: " + ypp);
    if (xpp > size-1){
        xpp--;
    }

    if (ypp > size-1){
        ypp--;
    }
    document.getElementById("x"+xpp+"y"+ypp).style.background = "blue";
}

function makeTable(size) {
    let pl = document.getElementById("raz");
    pl.innerText = "";
    let cell = [];
    let cont = 0;
    for (let i = 0; i < size; i++){
        let row = pl.insertRow();
        for (let j = 0; j < size; j++){
            cell[j] = row.insertCell(j);
            cell[j].id = "x" + cont++ + "y" + ((size-1)-i);
            //cell[j].style.padding =  (size/4) + "em";
        }
        cont = 0;
    }
}

function range() {
    size = document.getElementById("ran").value;
    makeTable(size);
    dot_place();
    document.getElementById("lbl_ran").innerText = "X/Y: " + size;
}

range();
showRast();